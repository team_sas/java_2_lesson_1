package ru.geekbrains;

import ru.geekbrains.competitors.Competitor;

public class Team {
    private String name;
    private Competitor[] members = new Competitor[4];

    Team (String name, Competitor m1, Competitor m2, Competitor m3, Competitor m4) {
        this.name = name;
        members[0] = m1;
        members[1] = m2;
        members[2] = m3;
        members[3] = m4;
    }

    public Competitor[] getMembers() {
        return members;
    }

    public void showResults() {
        System.out.println("Команда: " + name);
        for (Competitor member : members) {
            member.info();
        }
    }
}
