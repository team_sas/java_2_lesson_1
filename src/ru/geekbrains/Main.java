package ru.geekbrains;

import ru.geekbrains.competitors.animals.Cat;
import ru.geekbrains.competitors.animals.Dog;
import ru.geekbrains.competitors.humans.Human;
import ru.geekbrains.obstacle.Cross;
import ru.geekbrains.obstacle.Wall;
import ru.geekbrains.obstacle.Water;

public class Main {
    public static void main(String[] args) {
        Team team = new Team(
            "112",
            new Cat("Барсик"),
            new Dog("Бобик"),
            new Human("Илья Иванов"),
            new Human("Олег Петрович")
        );
        Course course = new Course(
            new Wall(2),
            new Cross(80),
            new Water(30)
        );
        course.doIt(team);
        team.showResults();
    }
}
