package ru.geekbrains;

import ru.geekbrains.competitors.Competitor;
import ru.geekbrains.obstacle.Obstacle;

public class Course {
    private Obstacle[] obstacles;

    Course (Obstacle...obstacles) {
        this.obstacles = obstacles;
    }

    public void doIt(Team team) {
        Competitor[] members = team.getMembers();
        for (Competitor member : members) {
            for (Obstacle obstacle : obstacles) {
                obstacle.doIt(member);
                if (!member.isOnDistance()) break;
            }
        }
    }
}
