package ru.geekbrains.obstacle;

import ru.geekbrains.competitors.Competitor;

public abstract class Obstacle {
    public abstract void doIt(Competitor competitor);
}
